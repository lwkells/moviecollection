Movie Collection REST Service  
-----------------------------  
  
By: Kelly Wilson  
  
This is a service to create and manage a movie collection.  
  
Each movie has the values:  
    - ID: a long  
	    - I considered making the ID a String and using UUIDs, but for the sake of simplicity I went with   
		long and incremented it with each movie addition. Obviously this isn't good for security, but with more time I would add authentication, encryption, and implement UUIDs.  
    - name: a String  
	    - The max character length for the database value is 100, but this would be easy to update in the table creation statement  
    - genre: a String  
	    - Max character length is also 100, like name.  
    - releaseYear: an int  
	    - I figured valid data will always input the year as a 4 digit number, so int can handle that fine   
	    - With more time I would add logic to check the year input to ensure it fits the 4 digit recquirement  
    - rating: a double  
	    - The application would define the rating system for the user, but I make the rating a double to account for a wide range of numbers
		that could also use decimals  
  
  
  
Endpoints   
----------  
  
GET /timeOfDay  
	- gives the time of day in the format of h:mm:ss  
	- example JSON:  
		{ "time" : "6:31:06" }  
  
GET /movie/list  
- returns a list of all movies in the database  
	- example JSON:  
		[  
			{   
				"id" : "1"  
				"name" : "Adventure Movie"  
				"genre" : "Adventure"  
				"releaseYear" : 1990  
				"rating" : 9.0  
			},  
			{    
				"id" : "2"  
				"name" : "Action Movie"  
				"genre" : "Action"  
				"releaseYear" : 2009  
				"rating" : 5.3  
			}  
		]  
  
Get a movie:  
GET /movie/{id}  
  
Create a movie:  
POST /movie  
	- example body:  
		{  
			"name" : "Adventure Movie"  
			"genre" : "Adventure"  
			"releaseYear" : 1990  
			"rating" : 9.0  
		}  
  
Update a movie:  
PUT /movie/{id}  
  
Delete a movie:  
DELETE /movie/{id}  
  
  
  
  
Exceptions  
-----------  
I created one specific exception to handle when the movie isn't found in the database. This can occur when attempting to  
get, update, or delete a movie. The exception is called "MovieNotFoundException".  

All other exceptions are handled by SpringBoot's default exception handling.  
