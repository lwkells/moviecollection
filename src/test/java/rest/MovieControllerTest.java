package rest;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import rest.MovieController;
import service.MovieManager;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(MovieController.class)
public class MovieControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private MovieManager movieManager;
	
	@Before
    public void setup() throws Exception {
		// Setup goes here
	}
	
	@Test
	public void testGetMoviesReturnsCorrectJSONList() throws Exception {
		Movie movie1 = new Movie(1, "Adventure Movie", "Adventure", 1995, 2.0);
		Movie movie2 = new Movie(2, "Action Movie", "Action", 2005, 6.7);
		List<Movie> movieList = Arrays.asList(movie1, movie2);
		when(movieManager.getMovieList()).thenReturn(movieList);
		
		mockMvc.perform(get("/movie/list"))
        	.andExpect(status().isOk())
        	.andExpect(jsonPath("$", hasSize(2)))
        	.andExpect(jsonPath("$[0].name", is(movie1.getName())))
        	.andExpect(jsonPath("$[1].name", is(movie2.getName())));
	}
	
	@Test
	public void testGetMovieWithValidIDReturnsCorrectJSON() throws Exception {
		Movie movie = new Movie(1, "Adventure Movie", "Adventure", 1995, 2.0);
		when(movieManager.getMovie(1)).thenReturn(movie);
		
		mockMvc.perform(get("/movie/1"))
        	.andExpect(status().isOk())
        	.andExpect(jsonPath("$.name", is(movie.getName())))
        	.andExpect(jsonPath("$.genre", is(movie.getGenre())))
        	.andExpect(jsonPath("$.releaseYear", is(movie.getReleaseYear())))
        	.andExpect(jsonPath("$.rating", is(movie.getRating())));
	}
	
	@Test
	public void testCreateMovieReturnsCorrectJSON() throws Exception {
		Movie movie = new Movie(1, "Adventure Movie", "Adventure", 1995, 2.0);
		
		mockMvc.perform(post("/movie")
				.contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(movie)))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.name", is(movie.getName())))
			.andExpect(jsonPath("$.genre", is(movie.getGenre())))
			.andExpect(jsonPath("$.releaseYear", is(movie.getReleaseYear())))
			.andExpect(jsonPath("$.rating", is(movie.getRating())));
	}
	
	@Test
	public void testUpdateMovieReturnsCorrectJSON() throws JsonProcessingException, Exception {
		Movie movie = new Movie(1, "Adventure Movie", "Adventure", 1995, 2.0);
		
		mockMvc.perform(post("/movie")
				.contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(movie)))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.name", is(movie.getName())))
			.andExpect(jsonPath("$.genre", is(movie.getGenre())))
			.andExpect(jsonPath("$.releaseYear", is(movie.getReleaseYear())))
			.andExpect(jsonPath("$.rating", is(movie.getRating())));
	}
	
	@Test
	public void testDeleteMovieReturnsCorrectJSON() throws JsonProcessingException, Exception {
		Movie movie = new Movie(1, "Adventure Movie", "Adventure", 1995, 2.0);
		
		mockMvc.perform(delete("/movie/1"))
			.andExpect(status().is(204));
	}
	
}
