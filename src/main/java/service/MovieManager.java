package service;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import rest.Movie;

public class MovieManager {
	
	@Autowired
	MovieDAO movieDAO;
	
	public void createMovie(Movie movie) throws SQLException {
		movieDAO.createMovie(movie);
	}
	
	public void updateMovie(Movie movie, long id) throws SQLException {
		doesMovieExist(id);
		movieDAO.updateMovie(movie, id);
	}
	
	public Movie getMovie(long id) throws SQLException {
		return movieDAO.getMovie(id);
	}
	
	public void deleteMovie(long id) throws SQLException {
		doesMovieExist(id);
		movieDAO.deleteMovie(id);
	}
	
	public List<Movie> getMovieList() throws SQLException {
		return movieDAO.getMovieList();
	}
	
	// Check movie exists by calling the get method - will throw a movieNotFoundException
	private void doesMovieExist(long id) throws SQLException {
		movieDAO.getMovie(id);
	}
}
