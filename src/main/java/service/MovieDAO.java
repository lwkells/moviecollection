package service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import rest.Movie;

public class MovieDAO {
	
	Connection connection;
	PreparedStatement statement;
	
	public MovieDAO() throws SQLException, ClassNotFoundException {
        connection = DriverManager.getConnection("jdbc:h2:mem:~/test", "sa", "");
        Class.forName("org.h2.Driver");
	}
	
	public void createMovie(Movie movie) throws SQLException {
		statement = connection.prepareStatement("insert into movie (id, name, genre, release_year, rating) values(?, ?, ?, ?, ?)");
		statement.setInt(1, (int) movie.getId());
		statement.setString(2, movie.getName());
		statement.setString(3, movie.getGenre());
		statement.setInt(4, movie.getReleaseYear());
		statement.setDouble(5, movie.getRating());
		statement.executeUpdate();
	}
	
	public void updateMovie(Movie movie, long id) throws SQLException {
		statement = connection.prepareStatement("update movie set name = ?, genre = ?, release_year = ?, rating = ? where id = ?");
		statement.setString(1, movie.getName());
		statement.setString(2, movie.getGenre());
		statement.setInt(3, movie.getReleaseYear());
		statement.setDouble(4, movie.getRating());
		statement.setInt(5, (int) id);
		statement.executeUpdate();
	}
	
	public Movie getMovie(long id) throws SQLException {
		statement = connection.prepareStatement("select * from movie where id = ?");
		statement.setInt(1, (int) id);
		ResultSet rs = statement.executeQuery();
		
		if (!rs.isBeforeFirst()) {
			throw new MovieNotFoundException("Movie with id " + id + " was not found");
		}
		
		return buildMovieList(rs).get(0);
	}
	
	public void deleteMovie(long id) throws SQLException {
		statement = connection.prepareStatement("delete from movie where id = ?");
		statement.setInt(1, (int) id);
		statement.executeUpdate();
	}
	
	public List<Movie> getMovieList() throws SQLException {
		statement = connection.prepareStatement("select * from movie");
		ResultSet rs = statement.executeQuery();
		
		return buildMovieList(rs);
	}
	
	private List<Movie> buildMovieList(ResultSet rs) throws SQLException {
		List<Movie> movieList = new ArrayList<Movie>();
		
		while(rs.next()) {
			Movie movie = new Movie();
			movie.setId(rs.getInt("id"));
			movie.setName(rs.getString("name"));
			movie.setGenre(rs.getString("genre"));
			movie.setReleaseYear(rs.getInt("release_year"));
			movie.setRating(rs.getDouble("rating"));
			
			movieList.add(movie);
		}
		
		return movieList;
	}

}
