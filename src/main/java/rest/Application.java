package rest;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        SpringApplication.run(Application.class, args);
        
        Connection c = DriverManager.getConnection("jdbc:h2:mem:~/test", "sa", "");
        Class.forName("org.h2.Driver");
        createDatabase(c);
        c.close();
    }
    
    public static void createDatabase(Connection c) throws SQLException {
    	Statement s = c.createStatement();
    	s.execute("create table movie(id int primary key, name varchar(100), genre varchar(100), release_year int, rating double)");
    	s.close();
    }
    
}
