package rest;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import service.MovieManager;

@RestController
public class MovieController {
    private final AtomicLong counter = new AtomicLong();
    
    @Autowired
    MovieManager movieManager;
    
    @GetMapping("/timeOfDay")
    public Map<String, Object> getCurrentTime() {
    	Map<String, Object> timeMap = new HashMap<String, Object>();
    	timeMap.put("time", new SimpleDateFormat("h:mm:ss").format(new Date()));
    	return timeMap;
    }
    
    @GetMapping("/movie/list")
    public List<Movie> getMovieList() throws SQLException {
		return movieManager.getMovieList();
    }
    
    @GetMapping("/movie/{id}")
    public Movie getMovie(@PathVariable long id) throws SQLException {
    	return movieManager.getMovie(id);
    }
    
    @PostMapping("/movie")
    public Movie createMovie(@RequestBody Movie movie) throws SQLException {
    	movie.setId(counter.incrementAndGet());
    	movieManager.createMovie(movie);
    	return movie;
    }
    
    @PutMapping("/movie/{id}")
    public Movie updateMovie(@RequestBody Movie movie, @PathVariable long id) throws SQLException {
    	movieManager.updateMovie(movie, id);
    	return movieManager.getMovie(id);
    }
    
    @DeleteMapping("/movie/{id}")
    public ResponseEntity<Object> deleteMovie(@PathVariable long id) throws SQLException {
    	movieManager.deleteMovie(id);
    	return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
    }
}
