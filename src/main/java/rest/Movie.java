package rest;

public class Movie {
	
	private long id;
	private String name;
	private String genre;
	private int releaseYear;
	private double rating;
	
	public Movie() {
		
	}
	
	public Movie(long id, String name, String genre, int releaseYear, double rating) {
		this.id = id;
		this.name = name;
		this.genre = genre;
		this.releaseYear = releaseYear;
		this.rating = rating;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getGenre() {
		return genre;
	}
	
	public void setGenre(String genre) {
		this.genre = genre;
	}
	
	public int getReleaseYear() {
		return releaseYear;
	}
	
	public void setReleaseYear(int releaseYear) {
		this.releaseYear = releaseYear;
	}
	
	public double getRating() {
		return rating;
	}
	
	public void setRating(double rating) {
		this.rating = rating;
	}

}
