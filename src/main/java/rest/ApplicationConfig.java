package rest;

import java.sql.SQLException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfig {

	@Bean
	public service.MovieManager MovieManager() {
		return new service.MovieManager();
	}
	
	@Bean
	public service.MovieDAO MovieDAO() throws ClassNotFoundException, SQLException {
		return new service.MovieDAO();
	}
}
